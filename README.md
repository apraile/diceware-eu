<!--
SPDX-FileCopyrightText: 2017 Aitor Fraile Azcue <aitorfraile@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Diceware euskaraz
[![REUSE status](https://api.reuse.software/badge/gitlab.com/apraile/diceware-eu)](https://api.reuse.software/info/gitlab.com/apraile/diceware-eu)

[_Diceware_](https://en.wikipedia.org/wiki/Diceware) seguruak eta gogoratzeko errazak diren pasahitzak sortzeko metodo bat da. [_Diceware_](http://world.std.com/~reinhold/diceware.html) web orrialdean agertzen diren pausuak jarraituz eta euskarazko hitz zerrenda erabilita ondorengoa bezalako pasahitzak lortu daitezke:

`etxarte isun xamar plasma arroa findu`

Euskarazko hitz zerrenda [**TXT**](https://gitlab.com/apraile/diceware-eu/blob/main/diceware-eu.txt) eta [**PDF**](https://gitlab.com/apraile/diceware-eu/blob/main/diceware-eu.pdf) formatuetan dago eskuragarri. 

_Nahiz eta segurtasun mailarik altuena egiazko dado fisikoak jaurtiz lortzen den, pasahitzak online sortzeko aukera dago [zenbait](https://diceware.rempe.us/#basque) [webgunetan](https://ae7.st/g/) (**Diceware metodoaren asmatzaileak** [**ez du gomendatzen**](http://world.std.com/~reinhold/dicewarefaq.html#electronic))._

# Nola sortu hitz zerrenda

**diceware-eu.txt** hitz zerrenda sortzeko ondorengo pausoak jarraitu dira:

- Jaitsi [hbep.xml](http://www.ehu.eus/ehg/hbep.xml) fitxategia [Hiztegi Batua Euskal Prosan (HBEP)](http://www.ehu.eus/ehg/) web orrialdetik eta [dicewarekit.txt](http://world.std.com/~reinhold/dicewarekit.txt) fitxategia.
- **hbep2diceware-eu.py** programa erabili ondorengo agindua terminalean exekutatuz:

    ```
    python3 hbep2diceware-eu.py hbep.xml \
    | sort -k 3n -k 2nr \
    | head -n $(( 7776 - $(wc -w dicewarekit.txt | awk '{print $1}') )) \
    | awk '{print $1}' > language_list.txt
    ```

- Jarraitu [_Diceware Kit_](http://world.std.com/~reinhold/dicewarekit.html) web orrialdeko pausoak 11. puntutik aurrera.

# SHA256 checksums

```
a61f2a9c92a7195574e8989ef036b4b40ceff04707e1d44c27d53c8e113dee59  diceware-eu.txt
3cff472e1519ba8270fd69ece1a139c4caff0137d38d56312bd1cfef2f766269  diceware-eu.pdf
52bfd9bd25ace63f4a0caf35617f564ab54e285b219452cf71d9f586aad99c7b  hbep2diceware-eu.py
```

# Lizentzia
**diceware-eu.txt** eta **diceware-eu.pdf** hitz zerrendak [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) lizentziapean daude.

[Hiztegi Batua Euskal Prosan (HBEP)](http://www.ehu.eus/ehg/) hiztegiko datuak erabili dira [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.eu) lizentziari esker (© 2008, Sarasola, Salaburu, Landa, Ugarteburu).

*Diceware is a trademark of [Arnold G. Reinhold](http://world.std.com/~reinhold/).*
