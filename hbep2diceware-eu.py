#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2017 Aitor Fraile Azcue <aitorfraile@mailbox.org>
#
# SPDX-License-Identifier: Apache-2.0

# Programa honek Hiztegi Batua Euskal Prosan (HBEP) azaltzen diren
# zenbait hitz pantailaratzen ditu, hauen maiztasuna eta letra
# kopuruarekin batera.

import xml.etree.ElementTree as ZuhaitzElementua
import sys
import re

LetraMax = 7
LetraMin = 3

XmlFitxategia = sys.argv[1]
zuhaitza = ZuhaitzElementua.parse(XmlFitxategia)
erroa = zuhaitza.getroot()

for EHG_lemak in erroa.findall('EHG_lemak'):

    if EHG_lemak.findtext('lemak_batuaB') == "1":
        hitza = EHG_lemak.findtext('lemak_lema')

        if re.match("^[a-zA-Z]+$",
                    hitza) and LetraMin <= len(hitza) <= LetraMax:
            HitzaMaiz = EHG_lemak.findtext('lemak_maiz')
            print(hitza.lower(), HitzaMaiz, len(hitza))
